package com.personal.todo.helper.database;

public class ServiceConstantHelper {

	public static final String DATE_PATTERN="MM/dd/yyyy HH:mm:SS";
	public static final int CREATE_NOTE = 0;
	public static final String NOTE_CREATE = "createNote";
	public static final String NOTE_ID = "noteID";
	public static final int EDIT_NOTE = 1;
	public static final String NOTE_EDIT = "noteEdited";
			
}
