package com.personal.todo.dao;

import java.util.List;

import android.database.SQLException;

import com.personal.todo.model.Note;

/** 
 * Interface class for the database based notes operations.
 * Supports add,remove,edit and get operations
 * 
 * @author ankul
 *
 */
public interface NotesDAO {

	public Note addNote(Note note);
		
	public boolean removeNoteById(int noteID);

	public Note editNote(int noteID,Note note);
	
	public Note editNote(Note note);
	
	public Note getNoteByID(int noteID);
	
	public List<Note> getAllNotes();

	void openWritable() throws SQLException;

	void close();

	void openReadable() throws SQLException;
	
	List<Note> searchNoteByName(String searchString);
	
	List<Note> searchNoteByDescription(String searchString);
}
