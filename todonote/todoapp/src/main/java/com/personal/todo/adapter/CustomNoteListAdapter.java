package com.personal.todo.adapter;

import roboguice.inject.ContextSingleton;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.inject.Inject;
import com.personal.todo.R;
import com.personal.todo.model.Note;

@ContextSingleton
public class CustomNoteListAdapter extends ArrayAdapter<Note> {

	private static final int DEFAULT_RESOURCE_ID = R.layout.list_row_layout;

	private int resourceID;
	
	@Inject
	LayoutInflater inflator;

	@Inject
	public CustomNoteListAdapter(Context context) {
		super(context, DEFAULT_RESOURCE_ID);
		setResourceID(DEFAULT_RESOURCE_ID);
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (getResourceID() == DEFAULT_RESOURCE_ID) {
			Log.d(CustomNoteListAdapter.class.toString(),
					"Using default notes resource ID");
		}
		if (isEmpty()) {
			Log.d(CustomNoteListAdapter.class.toString(),
					"There is not list element present.Use Add methods to add elements in the list");
		}
		View rowView = inflator.inflate(getResourceID(), parent, false);
		TextView noteName = (TextView) rowView.findViewById(R.id.noteName);
		noteName.setText(getItem(position).getName().toString());
		rowView.setTag(R.string.notesID, getItem(position).getId().toString());
		return rowView;
	}

	public int getResourceID() {
		return resourceID;
	}

	public void setResourceID(int resourceID) {
		this.resourceID = resourceID;
	}

}
