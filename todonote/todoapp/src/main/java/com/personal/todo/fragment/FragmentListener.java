package com.personal.todo.fragment;

import android.os.Bundle;

public interface FragmentListener {

	public void onFragmentComplete(Bundle extraInfo);
}
