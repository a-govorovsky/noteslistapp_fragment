package com.personal.todo.content.provider.note;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.google.inject.Inject;
import com.personal.todo.helper.database.MySQLHelper;

public class NotesContentProvider extends ContentProvider {

	private static final int NOTES = 0;
	private static final int NOTES_ITEM = 1;
	private static UriMatcher uriMatcher;

	@Inject
	private MySQLHelper dbHelper;
	private final String className = NotesContentProvider.class.toString();

	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(NotesProvider.AUTHORITY, NotesProvider.Note.NOTE_ALL,
				NOTES);
		uriMatcher.addURI(NotesProvider.AUTHORITY, NotesProvider.Note.NOTE_ITEM
				+ "/#", NOTES_ITEM);
	}

	@Override
	public boolean onCreate() {
		
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();

		if (null == projection) {
			projection = MySQLHelper.ALL_COLUMNS;
		}
		Cursor query = null;
		switch (uriMatcher.match(uri)) {
		case NOTES:
			query = readableDatabase.query(MySQLHelper.TABLE_NOTES, projection,
					null, null, null, null, null);
			break;
		case NOTES_ITEM:
			String noteID = uri.getPathSegments().get(1);
			query = readableDatabase.query(MySQLHelper.TABLE_NOTES, projection,
					"_id=" + noteID, null, null, null, null);
			break;
		default:
			Log.w(className, "Unsupported URL");
			break;
		}
		Log.i(className,"data rows:" + query.getCount());
		query.moveToFirst();
		readableDatabase.close();
		
		return query;
	}

	@Override
	public String getType(Uri uri) {

		switch (uriMatcher.match(uri)) {
		case NOTES:
			return NotesProvider.Note.CONTENT_MIME_TYPE;
		case NOTES_ITEM:
			return NotesProvider.Note.CONTENT_MIME_ITEM_TYPE;
		default:
			throw new IllegalArgumentException("URL not supported:"
					+ uri.getEncodedPath());
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		if (null == values)
			return null;

		if (uriMatcher.match(uri) != NOTES)
			return null;
		long insert = db.insert(MySQLHelper.TABLE_NOTES, null, values);
		if (insert > 0) {
			return ContentUris.withAppendedId(
					NotesProvider.Note.CONTENT_ITEM_URL, insert);
		}
		db.close();
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		if (null == uri) {
			throw new IllegalArgumentException("Invalid URI:");
		}
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int deleteCount = 0;
		switch (uriMatcher.match(uri)) {
		case NOTES:
			// set the selection criteria
			if (null == selection) {
				selection = "1";
				selectionArgs = null;
			}

			break;
		case NOTES_ITEM:
			int notesID = Integer.valueOf(uri.getPathSegments().get(
					NotesProvider.Note.NOTES_ID_PATH_SEGMENT));
			selection = getSelectionClause(selection, notesID);
			break;	
		default:
			Log.e(className, "Unsupported URI");
			throw new IllegalArgumentException("Unsupported URI:" + uri.toString());

		}

		deleteCount = db.delete(MySQLHelper.TABLE_NOTES, selection,
				selectionArgs);
		Log.i(className, "Rows deleted:"
				+ deleteCount);
		db.close();
		return deleteCount;
	}

	private String getSelectionClause(String selection, int notesID) {
		if (null != selection)
			selection += " AND ";
		else
			selection = "";

		selection += MySQLHelper.COLUMN_ID + "=" + notesID;
		return selection;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		if (null == uri) {
			throw new IllegalArgumentException("Unsupported URI");

		}
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int updateCount = 0;
		if(null == selection)
		{
			Log.w(className, "selection is null.Setting arguments to null");
			selectionArgs = null;
		}
			
		switch (uriMatcher.match(uri)) {
		case NOTES:
			// update all the rows with the new value
			selection = null;
			selectionArgs = null;
			break;
		case NOTES_ITEM:
			//if selection is there but no selection args then throw error
			if(null != selection && null == selectionArgs)
				throw new IllegalArgumentException("No selection arguments specified");
			
			int notesID = Integer.valueOf(uri.getPathSegments().get(
					NotesProvider.Note.NOTES_ID_PATH_SEGMENT));
			selection = getSelectionClause(selection, notesID);
			break;
		default:
			throw new IllegalArgumentException("Unsupported URI:" + uri.toString());
		}
		
		updateCount = db.update(MySQLHelper.TABLE_NOTES, values, selection,
				selectionArgs);
		db.close();
		Log.i(className, "Rows udpated:"
				+ updateCount);
		return updateCount;
	}

}
