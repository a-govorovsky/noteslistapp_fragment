package com.personal.todo.content.provider.note;

import android.net.Uri;
import android.provider.BaseColumns;

public class NotesProvider {


	/**
	 * Content Provider base authority 
	 */
	public static final String AUTHORITY = "com.content.provider.note";
	
	/**
	 * Notes constant property
	 */
	public static final int NOTES_ID_TAG = 1; 
	 
	
	/**
	 * Private so that the class cannot be specified
	 */
	@SuppressWarnings("unused")
	private void NotesProvider(){}
	
	public static class Note implements BaseColumns
	{
			/**
			 * Define the schema for the provider calls
			 */
			private static final String SCHEMA = "content://";
			
			private static final String URL_SEPERATOR = "/";
			
			/**
			 * URI endpoint to be used to get all information
			 */
			public static final String NOTE_ALL = "notes";
			
			/**
			 * URI endpoint to get ID based information
			 */
			public static final String NOTE_ITEM = "notes";
			
			/**
			 * Content URI needed for accessing all details 
			 */
			public static final Uri CONTENT_URI = Uri.parse(SCHEMA + AUTHORITY + URL_SEPERATOR + NOTE_ALL);

			/**
			 * Content URI for accessing notes based on ID
			 */
			public static final Uri CONTENT_ITEM_URL = Uri.parse(SCHEMA + AUTHORITY + URL_SEPERATOR + NOTE_ITEM);
			
			/**
			 * URI pattern to match single note retrival based on notes ID.# is for the ID that needs to be specified
			 */
			
			public static final Uri CONTENT_ITEM_PATTERN = Uri.parse(SCHEMA + AUTHORITY + NOTE_ITEM + "/#");
			
			/**
			 * MIME Type for {@link #CONTENT_URI} for a list of notes
			 */
			public static final String CONTENT_MIME_TYPE = "vnd.android.cursor.dir/vnd.personal.note";
			
			/**
			 * MIME Type for {@link #CONTENT_URI} for a single note based on ID
			 */
			public static final String CONTENT_MIME_ITEM_TYPE = "vnd.android.cursor.item/vnd.personal.note";
			
			/**
			 * Path segment for the notes ID in the provided URI
			 */
			public static final int NOTES_ID_PATH_SEGMENT = 1;
		}
	
}
