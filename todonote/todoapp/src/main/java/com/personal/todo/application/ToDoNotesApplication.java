package com.personal.todo.application;

import roboguice.RoboGuice;
import android.app.Application;

import com.google.inject.util.Modules;
import com.personal.todo.modules.ApplicationModule;


public class ToDoNotesApplication extends Application	{

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		RoboGuice.setBaseApplicationInjector(
				this,
				RoboGuice.DEFAULT_STAGE,
				Modules.override(RoboGuice.newDefaultRoboModule(this)).with(
						new ApplicationModule()));
	}
}
