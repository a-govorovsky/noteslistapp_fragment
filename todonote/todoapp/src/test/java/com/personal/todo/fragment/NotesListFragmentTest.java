package com.personal.todo.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.MockUtil;

import roboguice.RoboGuice;
import roboguice.activity.RoboFragmentActivity;
import roboguice.test.RobolectricRoboTestRunner;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.TypeLiteral;
import com.google.inject.util.Modules;
import com.personal.todo.adapter.CustomNoteListAdapter;
import com.personal.todo.dao.NotesDAO;
import com.personal.todo.model.Note;
import com.xtremelabs.robolectric.Robolectric;

@RunWith(RobolectricRoboTestRunner.class)
public class NotesListFragmentTest {

	private NotesListFragment fragment = null;
	private DummyActivity context;

	private NotesDAO mockDao = Mockito.mock(NotesDAO.class);

	private LayoutInflater layoutInflater = null; 
	@Before
	public void setup() {
		fragment = new NotesListFragment();
		RoboGuice
				.setBaseApplicationInjector(
						Robolectric.application,
						RoboGuice.DEFAULT_STAGE,
						Modules.override(
								RoboGuice
										.newDefaultRoboModule(Robolectric.application))
								.with(new MyTestModule()));

		context = new DummyActivity();
		context.onCreate(null);
		layoutInflater = RoboGuice
				.getInjector(Robolectric.application)
				.getBinding(LayoutInflater.class).getProvider().get();
	}

	@Test
	public void check_view_injection_in_fragment_successful() {
		fragment.onCreate(null);
		

		fragment.onViewCreated(
				fragment.onCreateView(layoutInflater, null, null), null);
		Assert.assertNotNull(((NotesListFragment) fragment).listView);
	}

	@Test
	public void check_non_view_injection_in_fragment_successful() {
		fragment.onCreate(null);
		Assert.assertNotNull(fragment.notesAdapter);
		Assert.assertNotNull(fragment.notesDao);
		Assert.assertEquals(Boolean.TRUE,
				new MockUtil().isMock(fragment.notesDao));
	}
	
	@Test
	public void adapter_having_no_element()
	{	
		Mockito.when(mockDao.getAllNotes()).thenReturn(new ArrayList<Note>());
		fragment.onCreate(null);
		Assert.assertEquals(0,fragment.notesAdapter.getCount());
		Mockito.verify(mockDao,Mockito.times(1)).getAllNotes();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void adapter_having_one_element()
	{
		List<Note> value = new ArrayList<Note>();
		Note dummyNote = new Note();
		dummyNote.setName("test");
		dummyNote.setDescription("test");
		dummyNote.setDateCreated(Calendar.getInstance().getTime());
		dummyNote.setId(1);
		value.add(dummyNote);
		
		Mockito.when(mockDao.getAllNotes()).thenReturn(value );
		Robolectric.shadowOf(fragment.notesAdapter).add(dummyNote);
		fragment.onCreate(null);
		
		
		Assert.assertEquals(1,fragment.notesAdapter.getCount());
		Mockito.verify(mockDao,Mockito.times(1)).getAllNotes();
	}

	class DummyActivity extends RoboSherlockFragmentActivity {
		@Inject
		NotesListFragment injectedFragment;

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			injectedFragment.onAttach(DummyActivity.this);
		}
	}

	class MyTestModule extends AbstractModule {

		@Override
		protected void configure() {
			bind(NotesListFragment.class).toInstance(fragment);
			bind(new TypeLiteral<ArrayAdapter<Note>>() {
			}).to(CustomNoteListAdapter.class);
			bind(NotesDAO.class).toInstance(mockDao);

		}

	}
}
